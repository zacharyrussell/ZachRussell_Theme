<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Materialize
 */

get_header();?>

<?php //materialize_post_thumbnail()?>
<?php if(has_post_thumbnail()) : ?>
    <div class="hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');" >
        <div class="intro">
            <h2><?php the_field('intro_title'); ?></h2>
            <p><?php the_field('intro_text'); ?> </p>
            <a class="btn-large white-text pink darken-1" href=<?php the_field('intro_link_location'); ?> >Hire Me Today!</a>
        </div>
    </div>
<?php endif; ?>
<div class="brands" style="background: #eee">
    <div class="container">
        <span><?php the_field('brands_title');?></span>
        <?php if (have_rows('brands_logos')): ?>
        <div class="brand">
            <?php while (have_rows('brands_logos')): the_row();?>
			                <img src="<?php the_sub_field('logo');?>" />
			            <?php endwhile;?>
        </div>
        <?php endif;?>
    </div>

</div>
<div class="services">
    <div class="container">
        <h2><?php the_field('portfolio_title');?></h2>
        <p><?php the_field('portfolio_subtitle');?></p>
        <?php if (have_rows('portfolio_items')): ?>
                <div class="portfolio-items">
            <?php while (have_rows('portfolio_items')): the_row();?>
                <div class="row">
                    <?php if (get_row_index() % 2 == 0): ?>
                        <div class="col s12 m8 l7">
                            <img src="<?php the_sub_field('image');?>">
                        </div>
                        <div class="col s12 m4 offset-l1">
                            <h3><?php the_sub_field('name');?></h3>
                            <p><?php the_sub_field('description');?></p>
                            <a class="btn-large white-text pink darken-1" target=_blank href=<?php the_sub_field('link_location');?> >View Project</a>
                        </div>
                    <?php else: ?>
                        <div class="col s12 m4">
                            <h3><?php the_sub_field('name');?></h3>
                            <p><?php the_sub_field('description');?></p>
                            <a class="btn-large white-text pink darken-1" target=_blank href=<?php the_sub_field('link_location');?> >View Project</a>
                        </div>
                        <div class="col s12 m8 l7 offset-l1">
                            <img src="<?php the_sub_field('image');?>">
                        </div>
                    <?php endif;?>
                    </div><!-- .row -->
            <?php endwhile;?>
            </div>
        <?php endif;?>
        <h2><?php the_field('services_title');?></h2>
        <p><?php the_field('services_subtitle'); ?></p>
        <?php if(have_rows('services')): ?>
            <?php while(have_rows('services')): the_row(); ?>
                <?php echo (get_row_index() % 3 == 1 ? '<div class="row">' : ''); ?>
                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-content">
                            <span class="card-title"><?php the_sub_field('name'); ?></span>
                            <p><?php the_sub_field('description'); ?></p>
                        </div>
                        <div class="card-action">
                            <a  class="pink-text text-darken-1" href=<?php the_sub_field('page_link'); ?> >Learn More</a>
                        </div>
                    </div>
                </div>
                <?php echo(get_row_index() % 3 == 0 ? '</div>' : ''); ?>
            <?php endwhile; ?>
        <?php endif; ?>

    </div>

<?php
get_footer();
