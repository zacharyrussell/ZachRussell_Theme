<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Materialize
 */

?>

	</div><!-- #content -->

    <footer id="colophon" class="page-footer <?php
do_action('materialize_get_color_scheme');?> ">
        <div class="container">
                <p class="center-align" style="margin-bottom: 0; margin-top: 0; padding: 1em 0;">Copyright &copy; <?php echo Date('Y'); ?> Zach Russell. </p>
            </div><!-- .site-info -->
        </div><!-- .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<?php wp_nav_menu( array(
    'theme_location'    => 'menu-1',
    'menu_id'           => 'slide-out',
    'menu_class'        => 'sidenav',
    'container'         => '',
)); ?>

</body>
</html>
