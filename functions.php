<?php
/**
 * Materialize functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Materialize
 */

if ( ! function_exists( 'materialize_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function materialize_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Materialize, use a find and replace
		 * to change 'materialize' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'materialize', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
					'menu-1' => esc_html__( 'Primary', 'materialize' ),
					) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
					'search-form',
					'comment-form',
					'comment-list',
					'gallery',
					'caption',
					) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'materialize_custom_background_args', array(
						'default-color' => 'ffffff',
						'default-image' => '',
						) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
					'height'      => 250,
					'width'       => 250,
					'flex-width'  => true,
					'flex-height' => true,
					) );
	}
endif;
add_action( 'after_setup_theme', 'materialize_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function materialize_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'materialize_content_width', 640 );
}
add_action( 'after_setup_theme', 'materialize_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function materialize_widgets_init() {
	register_sidebar( array(
				'name'          => esc_html__( 'Sidebar', 'materialize' ),
				'id'            => 'sidebar-1',
				'description'   => esc_html__( 'Add widgets here.', 'materialize' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h4 class="widget-title">',
				'after_title'   => '</h4>',
				) );
}
add_action( 'widgets_init', 'materialize_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function materialize_scripts() {
	wp_enqueue_style( 'materialize-style', get_stylesheet_uri() );
	wp_enqueue_style( 'materialize', '//cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css', array(), ' 1.0.0-');
	wp_enqueue_style('materialize-icons', '//fonts.googleapis.com/icon?family=Material+Icons');
	wp_enqueue_script('materialize', '//cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js', array(), '1.0.0-beta', true);

	wp_enqueue_script( 'materialize-navigation', get_template_directory_uri() . '/js/nav-new.js', array('materialize', 'jquery'), '20151215', true );

	wp_enqueue_script( 'materialize-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'materialize_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


add_filter('materialize_get_color_scheme', 'materialize_color_scheme');
function materialize_color_scheme() {
	echo "cyan ";
}

add_filter( 'gform_field_css_class', 'custom_class', 10, 3  );
function custom_class( $classes, $field, $form  ) {
	if ( $field->type == 'textarea'  ) {
		$classes .= ' custom_textfield_class';
	}
	return $classes;

}

add_image_size('blog-grid', 300, 200, true);

function materialize_excerpt_length( $length  ) {
	return 30;

}
add_filter( 'excerpt_length', 'materialize_excerpt_length', 999  );


// filter the Gravity Forms button type
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2  );
function form_submit_button( $button, $form  ) {
	return "<button class='btn button' id='gform_submit_button_{$form['id']}'>Submit</button>";

}

add_filter( 'gform_field_content', function ( $field_content, $field  ) {
		if ( $field->type == 'textarea'  ) {
		return str_replace( 'textarea medium', 'materialize-textarea', $field_content  );

		}
		return $field_content;
		}, 10, 2  );

add_filter('acf/settings/save_json', 'materialize_acf_json_save_point');

function materialize_acf_json_save_point( $path ) {

	// update path
	$path = get_stylesheet_directory() . '/acf-json';


	// return
	return $path;

}

add_filter('acf/settings/load_json', 'materialize_acf_json_load_point');

function materialize_acf_json_load_point( $paths ) {

	// remove original path (optional)
	unset($paths[0]);


	// append path
	$paths[] = get_stylesheet_directory() . '/acf-json';


	// return
	return $paths;

}

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' ); 

add_filter( 'gform_field_css_class', 'materialize_field_class', 10, 3 );

function materialize_field_class( $classes, $field, $form ) {
	if ( $field->type == 'text' ) {
		$classes .= ' input-field';	
	}
	return $classes;
}
