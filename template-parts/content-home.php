<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Materialize
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('col s12 m4'); ?>>
    <div class="card">
        <div class="card-image">
            <a href="<?php the_permalink(); ?>">
            <?php if( has_post_thumbnail() != '' ): ?>
                <?php the_post_thumbnail( 'blog-grid' ); ?>
            <?php else: ?>
                <img src="https://unsplash.it/300/200" />
            <?php endif; ?>
            </a>
        </div>
        <div class="card-content entry-content">
            <span class="card-title"> <?php the_title( '<h1
class="entry-title"><a href="' . get_permalink() . '">', '</a></h1>' ); ?></span>
            <?php
                the_excerpt();

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'materialize' ),
                    'after'  => '</div>',
                ) );
            ?>
        </div><!-- .entry-content -->
        <div class="card-action">
            <a class="pink-text text-darken-1" href="<?php the_permalink(); ?>">Read Article</a>

        </div>
        </div><!-- .card -->
</article><!-- #post-<?php the_ID(); ?> -->
